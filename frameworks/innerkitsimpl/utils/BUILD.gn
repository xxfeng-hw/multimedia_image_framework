# Copyright (C) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/multimedia/image_framework/ide/image_decode_config.gni")

if (use_clang_ios) {
  ohos_source_set("image_utils") {
    include_dirs = [
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils/include",
      "//foundation/multimedia/utils/include",
      "//foundation/multimedia/image_framework/plugins/manager/include",
      "//foundation/multimedia/image_framework/interfaces/innerkits/include",
      "//commonlibrary/c_utils/base/include",
      "//foundation/multimedia/media_utils_lite/interfaces/kits",
      "//foundation/communication/ipc/utils/include",
    ]

    sources = [
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils/src/image_trace.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils/src/image_utils.cpp",
      "src/image_system_properties.cpp",
      "src/image_type_converter.cpp",
    ]

    defines = image_decode_ios_defines
    sources -= [ "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils/src/image_trace.cpp" ]
    include_dirs += [ "$image_subsystem/mock/native/include/log" ]
    deps = [
      "//foundation/multimedia/image_framework/mock/native:log_mock_static",
      "//foundation/multimedia/image_framework/plugins/manager:pluginmanager",
    ]

    #relative_install_dir = "module/multimedia"
    subsystem_name = "multimedia"
    part_name = "image_framework"
  }
} else {
  ohos_shared_library("image_utils") {
    if (!use_clang_android) {
      sanitize = {
        cfi = true
        cfi_cross_dso = true
        debug = false
      }
    }
    include_dirs = [
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils/include",
      "//foundation/multimedia/utils/include",
      "//foundation/multimedia/image_framework/plugins/manager/include",
      "//foundation/multimedia/image_framework/interfaces/innerkits/include",
      "//commonlibrary/c_utils/base/include",
      "//foundation/multimedia/media_utils_lite/interfaces/kits",
      "//foundation/communication/ipc/utils/include",
      "$skia_root/skia",
      "${graphic_subsystem}/interfaces/inner_api/surface",
    ]

    sources = [
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils/src/image_trace.cpp",
      "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils/src/image_utils.cpp",
      "src/image_system_properties.cpp",
      "src/image_type_converter.cpp",
    ]

    if (use_mingw_win) {
      defines = image_decode_windows_defines
      sources -= [ "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils/src/image_trace.cpp" ]
      include_dirs +=
          [ "//foundation/multimedia/image_framework/mock/native/include" ]
      deps = [
        "//foundation/multimedia/image_framework/mock/native:log_mock_static",
        "//foundation/multimedia/image_framework/plugins/manager:pluginmanager_static",
      ]
    } else if (use_clang_mac) {
      defines = image_decode_mac_defines
      sources -= [ "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils/src/image_trace.cpp" ]
      include_dirs += [
        "//foundation/multimedia/image_framework/mock/native/include",
        "//third_party/bounds_checking_function/include",
      ]
      deps = [
        "//foundation/multimedia/image_framework/mock/native:log_mock_static",
        "//foundation/multimedia/image_framework/plugins/manager:pluginmanager_static",
        "//third_party/bounds_checking_function:libsec_static",
      ]
    } else if (use_clang_android) {
      defines = image_decode_android_defines
      sources -= [ "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils/src/image_trace.cpp" ]
      include_dirs += [ "$image_subsystem/mock/native/include/log" ]
      deps = [
        "//commonlibrary/c_utils/base:utils",
        "//foundation/multimedia/image_framework/mock/native:log_mock_static",
        "//foundation/multimedia/image_framework/plugins/manager:pluginmanager",
      ]
    } else {
      defines = [ "DUAL_ADAPTER" ]

      deps = [
        "//foundation/multimedia/image_framework/plugins/manager:pluginmanager",
      ]

      external_deps = [
        "c_utils:utils",
        "graphic_2d:color_manager",
        "graphic_surface:surface",
        "hilog:libhilog",
        "hitrace:hitrace_meter",
      ]

      if (is_standard_system) {
        external_deps += [ "init:libbegetutil" ]
      } else {
        external_deps += [ "startup:syspara" ]
      }
    }

    #relative_install_dir = "module/multimedia"
    subsystem_name = "multimedia"
    innerapi_tags = [ "platformsdk_indirect" ]
    part_name = "image_framework"
  }
}

ohos_static_library("image_utils_static") {
  include_dirs = [
    "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils/include",
    "//foundation/multimedia/utils/include",
    "//foundation/multimedia/image_framework/plugins/manager/include",
    "//foundation/multimedia/image_framework/interfaces/innerkits/include",
    "//commonlibrary/c_utils/base/include",
    "$skia_root/skia",
  ]

  sources = [
    "src/image_system_properties.cpp",
    "src/image_type_converter.cpp",
    "src/image_utils.cpp",
  ]

  if (use_mingw_win) {
    defines = image_decode_windows_defines
    include_dirs +=
        [ "//foundation/multimedia/image_framework/mock/native/include" ]
    deps = [
      "//foundation/multimedia/image_framework/mock/native:log_mock_static",
      "//foundation/multimedia/image_framework/plugins/manager:pluginmanager_static",
    ]
  } else if (use_clang_mac) {
    defines = image_decode_mac_defines
    include_dirs += [
      "//foundation/multimedia/image_framework/mock/native/include",
      "//third_party/bounds_checking_function/include",
    ]
    deps = [
      "//foundation/multimedia/image_framework/mock/native:log_mock_static",
      "//foundation/multimedia/image_framework/plugins/manager:pluginmanager_static",
      "//third_party/bounds_checking_function:libsec_static",
    ]
  } else if (use_clang_ios) {
    defines = image_decode_ios_defines
    include_dirs += [ "$image_subsystem/mock/native/include/log" ]
    deps = [
      "//foundation/multimedia/image_framework/mock/native:log_mock_static",
    ]
  } else if (use_clang_android) {
    defines = image_decode_android_defines
    include_dirs += [ "$image_subsystem/mock/native/include/log" ]
    deps = [
      "//foundation/multimedia/image_framework/mock/native:log_mock_static",
    ]
  } else {
    sources += [ "//foundation/multimedia/image_framework/frameworks/innerkitsimpl/utils/src/image_trace.cpp" ]
    deps = [
      "//foundation/multimedia/image_framework/plugins/manager:pluginmanager",
    ]

    external_deps = [
      "c_utils:utils",
      "hilog:libhilog",
      "hitrace:hitrace_meter",
    ]

    if (is_standard_system) {
      external_deps += [ "init:libbegetutil" ]
    } else {
      external_deps += [ "startup:syspara" ]
    }
  }
  subsystem_name = "multimedia"
  part_name = "image_framework"
}
